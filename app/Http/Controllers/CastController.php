<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.tampil', ['cast' => $cast]);
    }

    public function create(){
        return view('cast.tambah');
    }

    public function simpan(Request $request){
        $pesan =[
            'required' =>':Attribute  tidak boleh kosong',
            'required' =>':Attribute  tidak boleh kosong',
            'required' =>':Attribute  tidak boleh kosong'
        ];
         $request->validate([
            'nama' =>'required',
            'umur' =>'required',
            'bio' =>'required'
        ],$pesan);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']

        ]);
            return redirect('/cast')->with('alertTambah', 'Data berhasil ditambahkan');
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $id){
        $pesan =[
            'required' =>':Attribute  tidak boleh kosong',
            'required' =>':Attribute  tidak boleh kosong',
            'required' =>':Attribute  tidak boleh kosong'
        ];
         $request->validate([
            'nama' =>'required',
            'umur' =>'required',
            'bio' =>'required'
        ],$pesan);

        DB::table('cast')
        ->where('id', $id)
        ->update(
            [
                'nama' => $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio 
            ]
            );
            return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
