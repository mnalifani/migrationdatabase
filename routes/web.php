<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'home']);
Route::get('/cast', [CastController::class, 'index']);

//menampilkan data database
Route::get('/cast/create', [CastController::class, 'create']);

//menyimpan data ke database
Route::post('/cast', [CastController::class, 'simpan']);

//detail cast
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//update
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//detelet
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
