@extends('layout.master')

@section('judul')
    Detail Cast
@endsection


@section('content')
    <h1>{{$cast->nama}}</h1>
    <h5><i>{{$cast->umur}} tahun</i></h5>
    <p>{{$cast->bio}}</p>
    <a href="/cast" class="btn btn-info btn-sm">kembali</a>
@endsection
