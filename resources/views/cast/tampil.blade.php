@extends('layout.master')

@section('judul')
    Halaman Cast
@endsection


@section('content')
@if (session('alertTambah'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    {{session('alertTambah')}}
  </div>


    
@endif
    <a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
    
    <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Action</th>
        
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=> $value)
                <tr>
                    <td>{{$key +1}}</td>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->umur}}</td>
                    <td>
                        <form action="/cast/{{$value->id}}" method="post" onsubmit="return confirm('Yakin data akan dihapus?')">
                            @csrf
                            @method('DELETE')
                            <a href="cast/{{$value->id}}" class="btn btn-warning btn-sm">Detail</a>
                            
                            <a href="cast/{{$value->id}}/edit" class="btn btn-success btn-sm">Update</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Tidak ada data</td>
                </tr>
            @endforelse
        </tbody>
      </table>
@endsection